<?php
$site	= $this->konfigurasi_model->listing();
// include('produk.php');
// include('berita.php');
?>
<main id="main">
    <!-- ======= Cliens Section ======= -->
    <section id="cliens" class="cliens section-bg">
      <div class="container">

        <div class="row" data-aos="zoom-in">

          <div class="col-lg-4 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="<?php echo base_url('assets/template/img/clients/abn.jpg') ?>" class="img-fluid" alt="">
          </div>

          <div class="col-lg-4 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="<?php echo base_url('assets/template/img/clients/BLESINDO.jpg') ?>" class="img-fluid" alt="">
          </div>

          <div class="col-lg-4 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="<?php echo base_url('assets/template/img/clients/imedin.png') ?>" class="img-fluid" alt="">
          </div>

          <div class="col-lg-4 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="<?php echo base_url('assets/template/img/clients/FITCARE.jpg') ?>" class="img-fluid" alt="">
          </div>

          <div class="col-lg-4 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="<?php echo base_url('assets/template/img/clients/FLEXIMED.jpg') ?>" class="img-fluid" alt="">
          </div>

          <div class="col-lg-4 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="<?php echo base_url('assets/template/img/clients/HONEYBEE.jpg') ?>" class="img-fluid" alt="">
          </div>

          <div class="col-lg-4 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="<?php echo base_url('assets/template/img/clients/N3.jpg') ?>" class="img-fluid" alt="">
          </div>

          <div class="col-lg-4 col-md-4 col-6 d-flex align-items-center justify-content-center">
            <img src="<?php echo base_url('assets/template/img/clients/ROSBY.jpg') ?>" class="img-fluid" alt="">
          </div>

        </div>

      </div>
    </section><!-- End Cliens Section -->

    <!-- ======= About Us Section ======= -->
    <section id="about" class="about">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Profil Perusahaan</h2>
        </div>

        <div class="row content">
          <div class="col-lg-6">
            <h2> Visi & Misi </h2>
            <p>
              Membangun dan mengembangkan perusahaan penyalur alat kesehatan skala nasional yang terpercaya  untuk rumah sakit,  instansi maupun masyarakat umum dengan mempunyai daya saing dan terkemuka serta menyajikan berbagai inovatif produk untuk  sebagai mitra pelanggan.
            </p>
            <ul>
              <li><i class="ri-check-double-line"></i> Memberikan produk yang inovatif dan harga saing yang wajar</li>
              <li><i class="ri-check-double-line"></i> Memberikan produk yang berkualitas dengan jaminan purna jual</li>
              <li><i class="ri-check-double-line"></i> Memberikan komitmen yang baik untuk customer</li>
              <li><i class="ri-check-double-line"></i> Membangun tim yang solid secara internal</li>
            </ul>
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0">
            <h2> Profil Perusahaan </h2>
            <p><?php echo $site['tentang'] ?></p>
            <!-- <a href="#" class="btn-learn-more">Learn More</a> -->
          </div>
        </div>

      </div>
    </section><!-- End About Us Section -->

    <!-- ======= Why Us Section ======= -->
    <section id="why-us" class="why-us section-bg">
      <div class="container-fluid" data-aos="fade-up">

        <div class="row">

          <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">

            <div class="content">
              <h3><strong>Layanan Berkualitas Tinggi</strong></h3>
              <p>
                Kami melakukannya untuk kebaikan masyarakat yang lebih besar.
              </p>
            </div>

            <div class="accordion-list">
              <ul>
                <li>
                  <a data-bs-toggle="collapse" class="collapse" data-bs-target="#accordion-list-1"><span>01</span> Manufaktur Lokal Kesehatan <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-1" class="collapse show" data-bs-parent=".accordion-list">
                    <p>
                      Penghasilan perusahaan yang dikenakan pajak seringkali ditentukan seperti penghasilan kena pajak bagi wajib pajak orang pribadi.
                    </p>
                  </div>
                </li>

                <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-2" class="collapsed"><span>02</span> Produk & Layanan <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-2" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      Instrumen Bedah, Alat Kesehatan, Perabot Rumah Sakit, Registrasi Produk, Regulasi Perawatan Kesehatan, Manajemen Mutu, Non Elektromedik, Elektromedik.
                    </p>
                  </div>
                </li>

                <!-- <li>
                  <a data-bs-toggle="collapse" data-bs-target="#accordion-list-3" class="collapsed"><span>03</span> Dolor sit amet consectetur adipiscing elit? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
                  <div id="accordion-list-3" class="collapse" data-bs-parent=".accordion-list">
                    <p>
                      Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis
                    </p>
                  </div>
                </li> -->

              </ul>
            </div>

          </div>

          <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style='background-image: url("<?php echo base_url('assets/template/img/med-about-image.png')?>");' data-aos="zoom-in" data-aos-delay="150">&nbsp;</div>
        </div>

      </div>
    </section><!-- End Why Us Section -->

    <!-- ======= Skills Section ======= -->
    <section id="skills" class="skills">
      <div class="container" data-aos="fade-up">

        <div class="row">
          <div class="col-lg-6 d-flex align-items-center" data-aos="fade-right" data-aos-delay="100">
            <img src="<?php echo base_url('assets/template/img/Pengadaan-Alat-Kesehatan-2.png') ?>" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0 content" data-aos="fade-left" data-aos-delay="100">
            <h3>Branding Produk</h3>
            <p class="fst-italic">
              Nagato menjadi salah satu pemasok produk kesehatan terpercaya di Bandung.
            </p>

            <div class="skills-content">

              <div class="progress">
                <span class="skill">PT Nagato <i class="val">100%</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>

              <div class="progress">
                <span class="skill">PT Imedine <i class="val">90%</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>

              <div class="progress">
                <span class="skill">PT BCN <i class="val">75%</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>

              <div class="progress">
                <span class="skill">Alat Kesehatan <i class="val">55%</i></span>
                <div class="progress-bar-wrap">
                  <div class="progress-bar" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
              </div>

            </div>

          </div>
        </div>

      </div>
    </section><!-- End Skills Section -->

    <!-- ======= Services Section ======= -->
    <section id="services" class="services section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Pelayanan</h2>
          <p>Kami Melakukan Pelayanan Yang Terbaik Untuk Konsumen</p>
        </div>

        <div class="row">
          <div class="col-xl-3 col-md-6 d-flex align-items-stretch" data-aos="zoom-in" data-aos-delay="100">
            <div class="icon-box">
              <div class="icon"><i class="bx bxl-dribbble"></i></div>
              <h4><a href=""><?php echo $site['judul_1'] ?></a></h4>
              <p><?php echo $site['pesan_1'] ?></p>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" data-aos="zoom-in" data-aos-delay="200">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-file"></i></div>
              <h4><a href=""><?php echo $site['judul_2'] ?></a></h4>
              <p><?php echo $site['pesan_2'] ?></p>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0" data-aos="zoom-in" data-aos-delay="300">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-tachometer"></i></div>
              <h4><a href=""><?php echo $site['judul_3'] ?></a></h4>
              <p><?php echo $site['pesan_3'] ?></p>
            </div>
          </div>

          <div class="col-xl-3 col-md-6 d-flex align-items-stretch mt-4 mt-xl-0" data-aos="zoom-in" data-aos-delay="400">
            <div class="icon-box">
              <div class="icon"><i class="bx bx-layer"></i></div>
              <h4><a href=""><?php echo $site['judul_4'] ?></a></h4>
              <p><?php echo $site['pesan_4'] ?></p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= Cta Section ======= -->
    

    <!-- ======= Portfolio Section ======= -->
    <section id="portfolio" class="portfolio">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Portfolio</h2>
          <p>Video Mengenai Produk Yang Tersedia di PT Nusamed Mega Hastosa</p>
        </div>

        <ul id="portfolio-flters" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100">
          <li data-filter="*" class="filter-active">Video</li>
          <!-- <li data-filter=".filter-app">App</li> -->
          
        </ul>
        
        <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
          <?php foreach($video as $video) { ?>
          <div class="col-lg-6 col-md-6 portfolio-item filter-app">
			<!-- <h4 class="text-center"><?php echo $video->judul ?></h4> -->

            <div class="embed-responsive embed-responsive-4by3">
			  <iframe src="https://www.youtube.com/embed/<?php echo $video->video ?>" width="100%" height="380" frameborder="0" allowfullscreen></iframe>
			</div>
          </div>
          <?php } ?>
        </div>
      </div>
    </section><!-- End Portfolio Section -->


    
    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2>Hubungi Kami</h2>
          <p>Kami sangat mudah didekati dan ingin berbicara dengan Anda. Jangan ragu untuk menelepon, mengirim email kepada kami</p>
        </div>

        <div class="row">

          <div class="col-lg-5 d-flex align-items-stretch">
            <div class="info">
              <div class="address">
                <i class="bi bi-geo-alt"></i>
                <h4>Alamat:</h4>
                <p><?php echo nl2br ($site['alamat'])?></p>
              </div>

              <div class="email">
                <i class="bi bi-envelope"></i>
                <h4>Email:</h4>
                <p><?php echo $site['email']?></p>
              </div>

              <div class="phone">
                <i class="bi bi-phone"></i>
                <h4>Telephone:</h4>
                <p><?php echo $site['telepon']?></p>
              </div>
              
              <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" style="border:0; width: 100%; height: 290px;" allowfullscreen></iframe> -->
            </div>

          </div>

          <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch">
            <div class="info">
              <div class="address">
                <i class="bi bi-clock"></i>
                <h4>Buka Jam :</h4>
                  <p>Senin - Jumat<br>
				     09:00 AM - 05:00 PM<br>
				     Minggu: Tutup</p>
              </div>
              <div class="phone">
                <i class="bi bi-phone"></i>
                <h4>Handphone / Whatsapp:</h4>
                <p><?php echo $site['hp']?></p>
              </div>
              <div>	
              		<?php echo $site['google_map']?>

              </div>
              <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" style="border:0; width: 100%; height: 290px;" allowfullscreen></iframe> -->
            </div>

          <!-- <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch">
            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="name">Your Name</label>
                  <input type="text" name="name" class="form-control" id="name" required>
                </div>
                <div class="form-group col-md-6">
                  <label for="name">Your Email</label>
                  <input type="email" class="form-control" name="email" id="email" required>
                </div>
              </div>
              <div class="form-group">
                <label for="name">Subject</label>
                <input type="text" class="form-control" name="subject" id="subject" required>
              </div>
              <div class="form-group">
                <label for="name">Message</label>
                <textarea class="form-control" name="message" rows="10" required></textarea>
              </div>
              <div class="my-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div>
              <div class="text-center"><button type="submit">Send Message</button></div>
            </form>
          </div>

        </div> -->

      </div>
    </section><!-- End Contact Section -->
  </main><!-- End #main -->
