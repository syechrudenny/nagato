
<?php
$nav_produk = $this->site_model->nav_produk();
$nav_berita = $this->site_model->nav_berita();
$nav_profil = $this->site_model->nav_profil();
$site = $this->konfigurasi_model->listing();
?>
<section id="why-us" class="why-us section-bg">
  <div class="container-fluid" data-aos="fade-up">
    <div class="row">
      <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">
        <div class="content">
          <h3><?php echo $title ?></h3>
          <p style="text-align: justify-all;">
            <?php echo $read->keterangan ?>
            <!-- <a class="btn btn-primary" href="<?php echo base_url() ?>assets/template/doc/BSM.pdf" role="button">Download Brosur</a>
            <a class="btn btn-success" href="https://api.whatsapp.com/send?phone=<?php echo $site['hp']?>&text=Saya%20tertarik%20untuk%pesan%20KIRIM.EMAIL" role="button">Pesan Sekarang</a> -->
          </p>
        </div>
      </div>

      <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style='background-image: url("<?php echo base_url('assets/upload/image/'.$read->gambar) ?>");' data-aos="zoom-in" data-aos-delay="150">&nbsp;</div>
    </div>


  </div>
</section><!-- End Why Us Section -->

	