<section id="portfolio" class="portfolio">
  <div class="container" data-aos="fade-up">
    <div class="section-title">
      <h2><?php echo $title ?></h2>
      <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
    </div>

    <ul id="portfolio-flters" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100">
      <li data-filter="*" class="filter-active">All</li>
      <!-- <li data-filter=".filter-app">App</li>
      <li data-filter=".filter-card">Card</li>
      <li data-filter=".filter-web">Web</li> -->
    </ul>

    <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
      <?php foreach($berita as $berita) { ?>
      <div class="col-lg-4 col-md-6 portfolio-item filter-app">
        <div class="portfolio-img"><a href="<?php echo base_url('berita/read/'.$berita->slug_berita) ?>" data-title="<?php echo $berita->nama_berita ?>"><img src="<?php echo base_url('assets/upload/image/'.$berita->gambar) ?>" class="img-fluid" alt=""></a></div>
        <div class="portfolio-info">
          <h4><?php echo $berita->nama_berita ?></h4>
          <p>App</p>
          <a href="assets/img/portfolio/portfolio-1.jpg" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="App 1"><i class="bx bx-plus"></i></a>
          <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
        </div>
      </div>
       <?php } ?>
      
    </div>

  </div>
</section><!-- End Portfolio Section -->

<!--gallery-->
