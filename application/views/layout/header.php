<!--  -->
<?php
$nav_produk	= $this->site_model->nav_produk();
$nav_berita	= $this->site_model->nav_berita();
$nav_profil	= $this->site_model->nav_profil();
?>
<header id="header" class="fixed-top " style="background-color: yellow">
	<div class="container d-flex align-items-center">

	  <h1 class="logo me-auto" style="color: #0672a5"><a href="<?php echo base_url() ?>"><?php echo $site['namaweb'] ?></a></h1>
	  <!-- Uncomment below if you prefer to use an image logo -->
	  <!-- <a href="index.html" class="logo me-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->

	  <nav id="navbar" class="navbar">
	    <ul>
	      <li><a class="nav-link scrollto active" href="<?php echo base_url() ?>">Beranda</a></li>
	      <li><a class="nav-link scrollto" href="<?php echo base_url('#about')?>">Profil</a></li>
	      <li class="dropdown"><a href="#"><span>Berita</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              
              <?php foreach($nav_berita as $nav_berita) { ?>
	          	<li><a class="hvr-bounce-to-bottom" href="<?php echo base_url('berita/read/'.$nav_berita->slug_berita) ?>"><?php echo $nav_berita->nama_berita ?></a></li>
	          <?php } ?>
            </ul>
          </li>
          <li class="dropdown"><a href="#"><span>Produk</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <?php foreach($nav_produk as $nav_produk) { ?>
		        <li><a class="hvr-bounce-to-bottom" href="<?php echo base_url('produk/kategori/'.$nav_produk->slug_kategori_produk) ?>"><?php echo $nav_produk->nama_kategori_produk ?></a></li>
		      <?php } ?>
            </ul>
          </li>
	      <li><a class="nav-link   scrollto" href="<?php echo base_url('#portfolio')?>">Portfolio</a></li>
	      <!-- <li><a class="nav-link scrollto" href="#team">Job Vacancies</a></li>
	      <li><a class="nav-link scrollto" href="#mitra">Form Request Mitra</a></li> -->
	      <li><a class="contactus scrollto" href="<?php echo base_url('#contact')?>">Hubungi Kami</a></li>
	    </ul>
	    <i class="bi bi-list mobile-nav-toggle"></i>
	  </nav>
	  <!-- .navbar -->

	</div>
</header>
<!-- End Header -->

<!-- ======= Hero Section ======= -->
<section id="hero" class="d-flex align-items-center">

<div class="container">
  <div class="row">
    <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
      <img src="<?php echo base_url('assets/upload/image/'.$site['logo']) ?>" alt="<?php echo $site['namaweb'] ?>" style="max-width: 75%" />
      <!-- <img src="<?php echo base_url('assets/template/img/clients/nagato.png') ?>" class="img-fluid" alt=""> -->
      <!-- <h1>Better Solutions For Your Business</h1> -->
      <h1><?php echo $site['tagline'] ?></h1>
      <!-- <h2>We are team of talented designers making websites with Bootstrap</h2> -->
      <div class="d-flex justify-content-center justify-content-lg-start">
        <a href="<?php echo base_url('#contact')?>" class="btn-contact-us scrollto">Hubungi Kami</a>
        <a href="<?php echo $site['video'] ?>" class="glightbox btn-watch-video"><i class="bi bi-play-circle"></i><span>Watch Video</span></a>
      </div>
    </div>
    <div class="col-lg-4 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
      <img src="<?php echo base_url('assets/template/img/nagato-about-us.png')?>" class="img-fluid animated" alt="">
    </div>
    
  </div>
</div>

</section>
  <!-- End Hero -->
