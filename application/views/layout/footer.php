
<?php
$nav_produk = $this->site_model->nav_produk();
$nav_berita = $this->site_model->nav_berita();
$nav_profil = $this->site_model->nav_profil();
$site = $this->konfigurasi_model->listing();
?>
<footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-6 footer-contact">
            <h3><?php echo $site['namaweb']?></h3>
            <p>
              <?php echo nl2br ($site['alamat'])?><br>
              <?php echo nl2br ($site['website'])?><br>
              <strong>Telepon:</strong><?php echo $site['telepon']?><br>
              <strong>Handphone:</strong><?php echo $site['hp']?><br>
              <strong>Email:</strong><?php echo $site['email']?><br>
            </p>
          </div>

          <div class="col-lg-2 col-md-2 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url() ?>">Home</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
              <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
            </ul>
          </div>

          <div class="col-lg-2 col-md-2 footer-links">
            <h4>Kategori Produk</h4>
            <ul>
              <?php 
				        $footer_produk	= $this->site_model->nav_produk();
        				foreach($footer_produk as $foprod) {
        			  ?>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url('produk/kategori/'.$foprod->slug_kategori_produk) ?>"><?php echo $foprod->nama_kategori_produk ?></a></li>
              <?php } ?>
            </ul>
          </div>

          <div class="col-lg-2 col-md-2 footer-links">
            <h4>Berita</h4>
            <ul>
              <?php 
        				$footer_berita	= $this->site_model->nav_berita();
        				foreach($footer_berita as $fober) {
        			  ?>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url('berita/read/'.$fober->slug_berita) ?>"><?php echo $fober->nama_berita ?></a></li>
              <?php } ?>
            </ul>
          </div>

          <!-- <div class="col-lg-2 col-md-2 footer-links">
            <h4>Profil</h4>
            <ul>
              <?php 
        				$footer_profil	= $this->site_model->nav_profil();
        				foreach($footer_profil as $foprof) {
        			?>
              <li><i class="bx bx-chevron-right"></i> <a href="<?php echo base_url('berita/read/'.$foprof->slug_berita) ?>"><?php echo $foprof->nama_berita ?></a></li>
              <?php } ?>
            </ul>
          </div> -->

          <div class="col-lg-2 col-md-4 footer-links">
            <h4>Jejaring Sosial Kami</h4>
            <!-- <p>Cras fermentum odio eu feugiat lide par naso tierra videa magna derita valies</p> -->
            <div class="social-links mt-3">
              <a href="<?php echo $site['twitter']?>" class="twitter"><i class="bx bxl-twitter"></i></a>
              <a href="<?php echo $site['facebook']?>" class="facebook"><i class="bx bxl-facebook"></i></a>
              <a href="<?php echo $site['instagram']?>" class="instagram"><i class="bx bxl-instagram"></i></a>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="container footer-bottom clearfix">
      <div class="copyright">
        &copy; Copyright <script>document.write( new Date().getUTCFullYear() );</script> <strong><span><?php echo $site['namaweb']?></span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/ -->
        Designed by <a href="https://bootstrapmade.com/"><?php echo $site['namaweb']?></a>
      </div>   
    </div>
 </footer><!-- End Footer -->
  <div id="preloader"></div>
  <a href="https://api.whatsapp.com/send?phone=<?php echo $site['hp']?>&text=Saya%20tertarik%20untuk%pesan%20KIRIM.EMAIL" class="back-to-top d-flex align-items-center justify-content-center"><img class="_9v93" alt="Halaman Utama WhatsApp" src="https://static.whatsapp.net/rsrc.php/yz/r/lOol7j-zq4u.svg"></a>

  <!-- Vendor JS Files -->
  <script src="<?php echo base_url('assets/template/vendor/aos/aos.js') ?>"></script>
  <script src="<?php echo base_url('assets/template/vendor/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/template/vendor/glightbox/js/glightbox.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/template/vendor/isotope-layout/isotope.pkgd.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/template/vendor/php-email-form/validate.js') ?>"></script>
  <script src="<?php echo base_url('assets/template/vendor/swiper/swiper-bundle.min.js') ?>"></script>
  <script src="<?php echo base_url('assets/template/vendor/waypoints/noframework.waypoints.js') ?>"></script>

  <!-- Template Main JS File -->
  <script src="<?php echo base_url('assets/template/js/main.js') ?>"></script>
<!-- footer -->
</body>
</html>