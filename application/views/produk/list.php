
<?php
$nav_produk = $this->site_model->nav_produk();
$nav_berita = $this->site_model->nav_berita();
$nav_profil = $this->site_model->nav_profil();
$site = $this->konfigurasi_model->listing();
?>
<section id="team" class="team section-bg">
  <div class="container" data-aos="fade-up">

    <div class="section-title">
      <h2><?php echo $title ?></h2>
      <p><?php echo $keywords ?></p>
      <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
    </div>

    <div class="row mb-3">
      <?php foreach($produk as $produk) { ?>
      <div class="col-lg-6 mb-3 mt-lg-0">
        <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
          <div class="pic"><img src="<?php echo base_url('assets/upload/image/'.$produk->gambar) ?>" class="img-fluid" alt=""></div>
          <div class="member-info">
            <h4><?php echo $produk->nama_produk ?></h4>
            <span>Stok : <?php echo $produk->stok ?></span>
            <!-- <p>Job Description</p> -->
            <!-- <p>Quisquam facilis cum velit laborum corrupti fuga rerum quia</p> -->
            <a href="<?php echo base_url('produk/read/'.$produk->slug_produk) ?>" class="btn-contact-us scrollto">Detail Produk</a>
            <div class="social">
              <a href="<?php echo $site['twitter']?>" class="twitter"><i class="ri-twitter-fill"></i></a>
              <a href="<?php echo $site['facebook']?>" class="facebook"><i class="ri-facebook-fill"></i></a>
              <a href="<?php echo $site['instagram']?>" class="instagram"><i class="ri-instagram-fill"></i></a>
            </div>
          </div>
        </div>
      </div>
      <?php } ?>
    </div>

    <!-- Pagination -->
    <?php 
      echo $this->pagination->create_links();
    ?>
  </div>
</section><!-- End Team Section -->
<!-- <section id="portfolio" class="portfolio">
  <div class="container" data-aos="fade-up">
    <div class="section-title">
      <h2><?php echo $title ?></h2>
      <p><?php echo $keywords ?></p>      
    </div>

    <ul id="portfolio-flters" class="d-flex justify-content-center" data-aos="fade-up" data-aos-delay="100">
      <li data-filter="*" class="filter-active">All</li>
      
    </ul>

    <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
      <?php foreach($produk as $produk) { ?>
      <div class="col-lg-4 col-md-6 portfolio-item filter-app">
        <div class="portfolio-img"><a href="<?php echo base_url('produk/read/'.$produk->slug_produk) ?>" data-title="<?php echo $produk->nama_produk ?>"><img src="<?php echo base_url('assets/upload/image/'.$produk->gambar) ?>" class="img-fluid" alt=""></a></div>
        <div class="portfolio-info">
          <h4><?php echo $produk->nama_produk ?></h4>
          <a href="<?php echo base_url('assets/upload/image/'.$produk->gambar) ?>" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="<?php echo $produk->nama_produk ?>"><i class="bx bx-plus"></i></a>
          <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
        </div>
      </div>
       <?php } ?>

    </div>

  </div>
</section> --><!-- End Portfolio Section -->

<script>
  // $(document).ready(function ()
  $(window).on('load', function()
  {
    // alert('ready');
    // location.hash = "anchor";
    var x = $('#hero').height();
    jQuery('html,body').animate({scrollTop: x}, 400); 
  });
</script>
