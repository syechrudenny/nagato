
<?php
$nav_produk = $this->site_model->nav_produk();
$nav_berita = $this->site_model->nav_berita();
$nav_profil = $this->site_model->nav_profil();
$site = $this->konfigurasi_model->listing();
?>
<section id="why-us" class="why-us section-bg">
  <div class="container-fluid" data-aos="fade-up">
    <div class="row">
      <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch  order-2 order-lg-1">
        <div class="content">
          <!-- <h3><?php echo $title ?></h3> -->
          <p >
            <?php echo $read->keterangan ?>
            <!-- <a class="btn btn-primary" href="<?php //echo base_url() ?>assets/template/doc/BSM.pdf" role="button">Download Brosur</a> -->
            <a class="btn btn-primary" href="<?php if ($read->pdf!='') {echo base_url('assets/upload/pdf/' . $read->pdf);}else{echo 'javascript:alert(`No file!`)';} ?>" role="button">Download Brosur</a>
            <a class="btn btn-success" href="https://api.whatsapp.com/send?phone=<?php echo $site['hp']?>&text=Saya%20tertarik%20untuk%pesan%20KIRIM.EMAIL" role="button">Pesan Sekarang</a>
          </p>
        </div>
      </div>

      <div class="col-lg-5 align-items-stretch order-1 order-lg-2 img" style='background-image: url("<?php echo base_url('assets/upload/image/'.$read->gambar) ?>");' data-aos="zoom-in" data-aos-delay="150">&nbsp;</div>
    </div>


  </div>
</section><!-- End Why Us Section -->


  <!--about-->
  <!-- <div class="about"> 
    <div class="container">
      <h3 class="title"><?php echo $title ?></h3>
      <div class="about-text">
        <div class="col-md-6 about-text-left">
          <img src="<?php echo base_url('assets/upload/image/'.$read->gambar) ?>" class="img-responsive" alt="<?php echo $title ?>"/>
        </div>
        <div class="col-md-6 about-text-right">
          <h4><?php echo $title ?></h4>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-bordered">
  <tr>
    <td>Harga</td>
    <td>: Rp. <?php echo number_format($read->harga,'0',',','.') ?></td>
  </tr>
  <tr>
    <td>Kategori produk</td>
    <td>: <?php echo $read->nama_kategori_produk ?></td>
  </tr>
  <tr>
    <td>Stok</td>
    <td>: <?php echo $read->stok ?></td>
  </tr>
  <tr>
    <td>Keterangan</td>
    <td>: <?php echo $read->keterangan ?></td>
  </tr>
</table>

        </div>
        <div class="clearfix"> </div>
      </div>
    </div>
</div> -->

<script>
  // $(document).ready(function ()
  $(window).on('load', function()
  {
    // alert('ready');
    // location.hash = "anchor";
    var x = $('#hero').height();
    jQuery('html,body').animate({scrollTop: x}, 400); 
  });
</script>