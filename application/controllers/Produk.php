<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {
	
	// Load database
	public function __construct(){
		parent::__construct();
		$this->load->model('konfigurasi_model');
		$this->load->model('produk_model');
		$this->load->model('kategori_produk_model');
	}
	
	// Index 
	public function index() {
		$site	= $this->konfigurasi_model->listing();
		$produk	= $this->produk_model->home();
		
		$data	= array( 'title'	=> 'Produk '.$site['namaweb'].' | '.$site['tagline'],
						 'keywords' => 'Produk '.$site['namaweb'].', '.$site['keywords'],
						 'produk'	=> $produk,
						 'isi'		=> 'produk/list');
		$this->load->view('layout/wrapper',$data); 
	}
	
	// Kategori 
	public function kategori($slug_kategori_produk) {

		$site				= $this->konfigurasi_model->listing();
		$kategori			= $this->kategori_produk_model->read($slug_kategori_produk);
		$id_kategori_produk	= $kategori->id_kategori_produk;
		// $produk				= $this->produk_model->kategori($id_kategori_produk);
		
		$this->load->library('pagination');

		$jumlah_data 			= $this->produk_model->jumlah_data();
		$cfg['base_url'] 	= base_url().'index.php/Produk/kategori/'.$slug_kategori_produk;
		$cfg['total_rows'] 	= $jumlah_data-1;
		$cfg['per_page'] 	= 5;

		$cfg['attributes'] = array('class' => 'page-link');
		$cfg['full_tag_open']   = '<ul class="pagination justify-content-center">';
        $cfg['full_tag_close']  = '</ul>';
        
        $cfg['first_link']      = ' <i class="bi bi-chevron-bar-left"></i> '; 
        $cfg['first_tag_open']  = '<li class="page-item">';
        $cfg['first_tag_close'] = '</li>';
        
        $cfg['last_link']       = ' <i class="bi bi-chevron-bar-right"></i> '; 
        $cfg['last_tag_open']   = '<li class="page-item">';
        $cfg['last_tag_close']  = '</li>';
        
        $cfg['next_link']       = ' <i class="bi bi-caret-right-fill"></i> '; 
        $cfg['next_tag_open']   = '<li class="page-item">';
        $cfg['next_tag_close']  = '</li>';
        
        $cfg['prev_link']       = ' <i class="bi bi-caret-left-fill"></i> '; 
        $cfg['prev_tag_open']   = '<li class="page-item">';
        $cfg['prev_tag_close']  = '</li>';
        
        $cfg['cur_tag_open']    = '<li class="page-item"><div class="page-link bg-primary text-white" href="javascript:void(0);">';
        $cfg['cur_tag_close']   = '</div></li>';
         
        // $cfg['num_tag_open']    = '<li>';
        // $cfg['num_tag_close']   = '</li>';
		
		$from 					= $this->uri->segment(4);
		// echo $from;
		
		$this->pagination->initialize($cfg);	
		
		$data	= array( 'title'	=> 'Kategori Produk '.$kategori->nama_kategori_produk,
						 'keywords' => $kategori->keterangan,
						//  'produk'	=> $produk,
						 'isi'		=> 'produk/list');
						 
		$data['produk']		= $this->produk_model->kategori($cfg['per_page'],$from,$id_kategori_produk);

		$this->load->view('layout/wrapper',$data); 
		// print_r($data);
	}
	
	// Kategori2 
	public function kategori2($slug_kategori_produk) {
		
		// $this->load->database();
		$this->load->library('pagination');

		$jumlah_data 			= $this->produk_model->jumlah_data();
		$cfg['base_url'] 	= base_url().'index.php/Produk/kategori2/'.$slug_kategori_produk;
		$cfg['total_rows'] 	= $jumlah_data-1;
		$cfg['per_page'] 	= 5;

		$cfg['attributes'] = array('class' => 'pagination');
		// $cfg['full_tag_open']   = '<ul class="pagination pagination-sm m-t-xs m-b-xs">';
        // $cfg['full_tag_close']  = '</ul>';
        
        $cfg['first_link']      = 'First'; 
        // $cfg['first_tag_open']  = '<li>';
        // $cfg['first_tag_open']  = '<li>';
        // $cfg['first_tag_close'] = '</li>';
        
        $cfg['last_link']       = 'Last'; 
        // $cfg['last_tag_open']   = '<li>';
        // $cfg['last_tag_close']  = '</li>';
        
        // $cfg['next_link']       = ' <i class="glyphicon glyphicon-menu-right"></i> '; 
        $cfg['next_link']       = ' Next '; 
        // $cfg['next_tag_open']   = '<li>';
        // $cfg['next_tag_close']  = '</li>';
        
        // $cfg['prev_link']       = ' <i class="glyphicon glyphicon-menu-left"></i> '; 
        $cfg['prev_link']       = ' Previous '; 
        // $cfg['prev_tag_open']   = '<li>';
        // $cfg['prev_tag_close']  = '</li>';
        
        // $cfg['cur_tag_open']    = '<li class="active"><a href="#">';
        // $cfg['cur_tag_close']   = '</a></li>';
         
        // $cfg['num_tag_open']    = '<li>';
        // $cfg['num_tag_close']   = '</li>';
		
		$from 					= $this->uri->segment(4);
		// echo $from;
		
		$this->pagination->initialize($cfg);	

		$kategori			= $this->kategori_produk_model->read($slug_kategori_produk);
		$id_kategori_produk	= $kategori->id_kategori_produk;
		// $data['produk']		= $this->produk_model->kategori($id_kategori_produk);
		$data['produk']		= $this->produk_model->kategori2($cfg['per_page'],$from,$id_kategori_produk);

		$this->load->view('produk/list3',$data); 
	}
	
	// Read
	public function read($slug_produk) {
		$site	= $this->konfigurasi_model->listing();
		$produk	= $this->produk_model->home();
		$read	= $this->produk_model->read($slug_produk);
		
		$data	= array( 'title'	=> $read->nama_produk,
						 'keywords' => $read->nama_produk,
						 'produk'	=> $produk,
						 'read'		=> $read,
						 'isi'		=> 'produk/read');
		$this->load->view('layout/wrapper',$data); 
	}
}
		